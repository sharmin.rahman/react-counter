A simple counter bootstrapped by create-react-app.

## To test the app:

	# To install dependencies
		npm install or, npm i

	# To serve on localhost:3000
		npm start

	# To build for production
		npm run build